<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();

$collection->add('actors_index', new Route(
    '/',
    array('_controller' => 'AppBundle:Actors:index'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('actors_show', new Route(
    '/{id}/show',
    array('_controller' => 'AppBundle:Actors:show'),
    array(),
    array(),
    '',
    array(),
    array('GET')
));

$collection->add('actors_new', new Route(
    '/new',
    array('_controller' => 'AppBundle:Actors:new'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('actors_edit', new Route(
    '/{id}/edit',
    array('_controller' => 'AppBundle:Actors:edit'),
    array(),
    array(),
    '',
    array(),
    array('GET', 'POST')
));

$collection->add('actors_delete', new Route(
    '/{id}/delete',
    array('_controller' => 'AppBundle:Actors:delete'),
    array(),
    array(),
    '',
    array(),
    array('DELETE')
));

return $collection;
