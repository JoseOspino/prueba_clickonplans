<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ActorsMovies;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Actorsmovie controller.
 *
 */
class ActorsMoviesController extends Controller
{
    /**
     * Lists all actorsMovie entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $actorsMovies = $em->getRepository('AppBundle:ActorsMovies')->findAll();

        return $this->render('actorsmovies/index.html.twig', array(
            'actorsMovies' => $actorsMovies,
        ));
    }

    /**
     * Creates a new actorsMovie entity.
     *
     */
    public function newAction(Request $request)
    {
        $actorsMovie = new Actorsmovies();
        $form = $this->createForm('AppBundle\Form\ActorsMoviesType', $actorsMovie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($actorsMovie);
            $em->flush();

            return $this->redirectToRoute('actors_show', array('id' => $actorsMovie->getId()));
        }

        return $this->render('actorsmovies/new.html.twig', array(
            'actorsMovie' => $actorsMovie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a actorsMovie entity.
     *
     */
    public function showAction(ActorsMovies $actorsMovie)
    {
        $deleteForm = $this->createDeleteForm($actorsMovie);

        return $this->render('actorsmovies/show.html.twig', array(
            'actorsMovie' => $actorsMovie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing actorsMovie entity.
     *
     */
    public function editAction(Request $request, ActorsMovies $actorsMovie)
    {
        $deleteForm = $this->createDeleteForm($actorsMovie);
        $editForm = $this->createForm('AppBundle\Form\ActorsMoviesType', $actorsMovie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('actors_edit', array('id' => $actorsMovie->getId()));
        }

        return $this->render('actorsmovies/edit.html.twig', array(
            'actorsMovie' => $actorsMovie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a actorsMovie entity.
     *
     */
    public function deleteAction(Request $request, ActorsMovies $actorsMovie)
    {
        $form = $this->createDeleteForm($actorsMovie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($actorsMovie);
            $em->flush();
        }

        return $this->redirectToRoute('actors_index');
    }

    /**
     * Creates a form to delete a actorsMovie entity.
     *
     * @param ActorsMovies $actorsMovie The actorsMovie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ActorsMovies $actorsMovie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('actors_delete', array('id' => $actorsMovie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
