<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActorsMovies
 *
 * @ORM\Table(name="actors_movies", indexes={@ORM\Index(name="FK_movie", columns={"movie_id"}), @ORM\Index(name="FK_actor", columns={"actor_id"})})
 * @ORM\Entity
 */
class ActorsMovies
{
    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50, nullable=true)
     */
    private $role;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Movies
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Movies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     * })
     */
    private $movie;

    /**
     * @var \AppBundle\Entity\Actors
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Actors")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actor_id", referencedColumnName="id")
     * })
     */
    private $actor;



    /**
     * Set role
     *
     * @param string $role
     *
     * @return ActorsMovies
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set movie
     *
     * @param \AppBundle\Entity\Movies $movie
     *
     * @return ActorsMovies
     */
    public function setMovie(\AppBundle\Entity\Movies $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \AppBundle\Entity\Movies
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set actor
     *
     * @param \AppBundle\Entity\Actors $actor
     *
     * @return ActorsMovies
     */
    public function setActor(\AppBundle\Entity\Actors $actor = null)
    {
        $this->actor = $actor;

        return $this;
    }

    /**
     * Get actor
     *
     * @return \AppBundle\Entity\Actors
     */
    public function getActor()
    {
        return $this->actor;
    }
}
