<?php

namespace 'App\Entity';

use Doctrine\ORM\Mapping as ORM;

/**
 * ActorsMovies
 *
 * @ORM\Table(name="actors_movies", indexes={@ORM\Index(name="FK_movie", columns={"movie_id"}), @ORM\Index(name="FK_actor", columns={"actor_id"})})
 * @ORM\Entity
 */
class ActorsMovies
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50, nullable=true)
     */
    private $role;

    /**
     * @var \Actors
     *
     * @ORM\ManyToOne(targetEntity="Actors")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actor_id", referencedColumnName="id")
     * })
     */
    private $actor;

    /**
     * @var \Movies
     *
     * @ORM\ManyToOne(targetEntity="Movies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     * })
     */
    private $movie;


}

